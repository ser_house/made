<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

Route::prefix('attributes')->group(function () {
  Route::post('/add', 'Attributes@add')->name('api_attributes_add');
  Route::post('/update', 'Attributes@update')->name('api_attributes_update');
  Route::post('/remove', 'Attributes@remove')->name('api_attributes_remove');
  Route::get('/listing', 'Attributes@listing')->name('api_attributes_listing');
  Route::get('/find', 'Attributes@find')->name('api_attributes_find');
});
