<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:08
 */


namespace Tests\Api;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase;
use Tests\CreatesApplication;

class BaseTestCase extends TestCase {
  use CreatesApplication;
  use DatabaseMigrations;
  use RefreshDatabase;
  // Если надо тестовую базу посмотреть после завершения тестов.
 //  Закомментировать use RefreshDatabase; и раскомментировать этот метод.
//	public function runDatabaseMigrations() {
//		$this->artisan('migrate:fresh');
//	}
  protected function setUp(): void {
    parent::setUp();

    $this->artisan('db:seed');
  }
}
