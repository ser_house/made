<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 9:53
 */

namespace Tests\Api\Attributes\Manage;

use Core\Attribute\Dictionary;
use Core\Attribute\ProductPropertyType;


class ProductPropertyTest extends ManageBaseTest {

  protected const TEST_ID = \ProductPropertySeeder::TEST_ID;
  protected const TEST_TITLE = 'Свойство';
  protected const TEST_DICTIONARY = Dictionary::PRODUCT_PROPERTY;

  public function testAddSuccess() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'title' => static::TEST_TITLE,
      'type' => \ProductPropertySeeder::TEST_TYPE,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(201);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddDuplicateFail() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'title' => \ProductPropertySeeder::TEST_TITLE,
      'type' => \ProductPropertySeeder::TEST_TYPE,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testUpdateTypeSuccess() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'id' => \ProductPropertySeeder::TEST_ID,
      'type' => ProductPropertyType::INTEGER,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(200);
    $response->assertJson(['type' => 'success']);
  }

  public function testUpdateDuplicateFail() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'id' => \ProductPropertySeeder::TEST_ID_2,
      'title' => \ProductPropertySeeder::TEST_TITLE,
      'type' => \ProductPropertySeeder::TEST_TYPE,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }
}
