<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 9:53
 */

namespace Tests\Api\Attributes\Manage;

use App\Attributes\Manage\Urls;
use Core\Infrastructure\Url\Add;
use Core\Infrastructure\Url\Listing;
use Core\Infrastructure\Url\Update;
use Illuminate\Support\Str;
use Tests\Api\BaseTestCase;

abstract class ManageBaseTest extends BaseTestCase {

  protected const TEST_ID = '';
  protected const TEST_TITLE = '';
  protected const TEST_DUPLICATE_TITLE = '';
  protected const TEST_DICTIONARY = '';

  /** @var Urls */
  protected $urls;

  protected function setUp(): void {
    parent::setUp();

    $this->urls = new Urls();
    $this->urls
      ->setAdd(new Add(route('api_attributes_add')))
      ->setUpdate(new Update(route('api_attributes_update')))
      ->setListing(new Listing(route('api_attributes_listing')));
  }

  public function testAddSuccess() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'title' => Str::random(50),
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(201);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddWithoutDictionaryFail() {
    $data = [
      'title' => static::TEST_TITLE,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testAddDuplicateFail() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'title' => static::TEST_DUPLICATE_TITLE,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testAddExceededMaxLenTitleFail() {
    $data = [
      'dictionary' => static::TEST_DICTIONARY,
      'title' => Str::random(500),
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testUpdateTitleSuccess() {
    $data = [
      'id' => static::TEST_ID,
      'dictionary' => static::TEST_DICTIONARY,
      'title' => Str::random(10),
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(200);
    $response->assertJson(['type' => 'success']);
  }

  public function testUpdateDuplicateFail() {
    $another_id = \DB::table(static::TEST_DICTIONARY)
      ->where('title', '!=', static::TEST_DUPLICATE_TITLE)
      ->limit(1)
      ->value('id');
    $data = [
      'id' => $another_id,
      'dictionary' => static::TEST_DICTIONARY,
      'title' => static::TEST_DUPLICATE_TITLE,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }
}
