<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 9:53
 */

namespace Tests\Api\Attributes\Manage;

use App\Model\UnitOfWork;
use Core\Attribute\Dictionary;


class RoomParamTest extends ManageBaseTest {

  protected const TEST_ID = \RoomParamSeeder::TEST_ID;
  protected const TEST_TITLE = 'Параметр';
  protected const TEST_DICTIONARY = Dictionary::ROOM_PARAM;

  public function testAddSuccess() {
    $unit = UnitOfWork::where('title', \RoomParamSeeder::TEST_UNIT)->first();
    $data = [
      'dictionary' => Dictionary::ROOM_PARAM,
      'title' => static::TEST_TITLE,
      'unit_of_work_id' => $unit->id,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(201);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddDuplicateFail() {
    $unit_id = \DB::table('unit_of_work')
      ->where('title', '=', \RoomParamSeeder::TEST_UNIT)
      ->limit(1)
      ->value('id');
    $data = [
      'dictionary' => Dictionary::ROOM_PARAM,
      'title' => \RoomParamSeeder::TEST_TITLE,
      'unit_of_work_id' => $unit_id,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testUpdateUnitSuccess() {
    $another_unit_id = \DB::table('unit_of_work')
      ->where('title', '!=', \RoomParamSeeder::TEST_UNIT)
      ->limit(1)
      ->value('id');
    $data = [
      'id' => \RoomParamSeeder::TEST_ID,
      'dictionary' => Dictionary::ROOM_PARAM,
      'unit_of_work_id' => $another_unit_id,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(200);
    $response->assertJson(['type' => 'success']);
  }

  public function testUpdateDuplicateFail() {
    $another_id = \DB::table('room_param')
      ->where('title', '!=', \RoomParamSeeder::TEST_TITLE)
      ->limit(1)
      ->value('id');
    $unit_id = \DB::table('unit_of_work')
      ->where('title', '=', \RoomParamSeeder::TEST_UNIT)
      ->limit(1)
      ->value('id');
    $data = [
      'dictionary' => Dictionary::ROOM_PARAM,
      'id' => $another_id,
      'title' => \RoomParamSeeder::TEST_TITLE,
      'unit_of_work_id' => $unit_id,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }
}
