<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 9:53
 */

namespace Tests\Api\Attributes\Manage;

use Core\Attribute\Dictionary;

class StageTest extends ManageBaseTest {

  protected const TEST_ID = \StageSeeder::TEST_ID;
  protected const TEST_TITLE = 'Этап';
  protected const TEST_DUPLICATE_TITLE = \StageSeeder::TEST_DUPLICATE_TITLE;
  protected const TEST_DICTIONARY = Dictionary::STAGE;
}
