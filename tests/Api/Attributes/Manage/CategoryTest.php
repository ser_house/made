<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 9:53
 */

namespace Tests\Api\Attributes\Manage;

use Core\Attribute\Dictionary;
use Illuminate\Support\Str;


class CategoryTest extends ManageBaseTest {

  protected const TEST_ID = \CategorySeeder::TEST_ID;
  protected const TEST_TITLE = \CategorySeeder::TEST_TITLE;
  protected const TEST_DICTIONARY = Dictionary::CATEGORY;

  public function testAddRootSuccess() {
    $data = [
      'dictionary' => Dictionary::CATEGORY,
      'title' => Str::random(50),
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(201);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddChildSuccess() {
    $data = [
      'dictionary' => Dictionary::CATEGORY,
      'title' => Str::random(50),
      'parent_id' => \CategorySeeder::TEST_PARENT_ID,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(201);
    $response->assertJson(['type' => 'success']);
  }

  public function testAddDuplicateFail() {
    $data = [
      'dictionary' => Dictionary::CATEGORY,
      'title' => \CategorySeeder::TEST_TITLE,
    ];
    $response = $this->postJson($this->urls->add, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }

  public function testUpdateParentSuccess() {
    $another_parent_id = \DB::table('category')
      ->where('title', '!=', \CategorySeeder::TEST_TITLE)
      ->limit(1)
      ->value('id');
    $data = [
      'id' => \CategorySeeder::TEST_ID,
      'dictionary' => Dictionary::CATEGORY,
      'parent_id' => $another_parent_id,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(200);
    $response->assertJson(['type' => 'success']);
  }

  public function testUpdateDuplicateFail() {
    $another_id = \DB::table('category')
      ->where('title', '!=', \CategorySeeder::TEST_TITLE)
      ->limit(1)
      ->value('id');
    $data = [
      'dictionary' => Dictionary::CATEGORY,
      'id' => $another_id,
      'title' => \CategorySeeder::TEST_TITLE,
    ];
    $response = $this->postJson($this->urls->update, $data);
    $response->assertStatus(400);
    $response->assertJson(['type' => 'error']);
  }
}
