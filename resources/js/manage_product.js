require('./bootstrap');

window.Vue = require('vue');

Vue.config.productionTip = false;
Vue.component('manage-product', require('../js/components/manage/Product.vue').default);

// https://paliari.github.io/v-toaster/
import Toaster from 'v-toaster'

Vue.use(Toaster, {timeout: 10000});

// axios должен считать нормальными любые http-коды, полученные с сервера
window.axios.defaults.validateStatus = function (status) {
  return true;
};

function trimEmptyTextNodes (el) {
  for (let node of el.childNodes) {
    if (node.nodeType === Node.TEXT_NODE && node.data.trim() === '') {
      node.remove()
    }
  }
}

Vue.directive('trim-whitespace', {
  inserted: trimEmptyTextNodes,
  componentUpdated: trimEmptyTextNodes
});

const app = new Vue({
  el: '#app',
});
