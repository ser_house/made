export let InlineEdit = {
  computed: {
    placeholder() {
      let maxLen = null;
      if ('undefined' !== typeof this.data && 'undefined' !== typeof this.data.max_title_len) {
        maxLen = this.data.max_title_len;
      }
      else if ('undefined' !== typeof this.maxTitleLen) {
        maxLen = this.maxTitleLen;
      }

      if (null !== maxLen) {
        return 'До ' + maxLen + ' символов.';
      }

      return '';
    },
    hasEditing() {
      for(let i in this.items) {
        if (this.items[i].is_editing) {
          return true;
        }
      }

      return false;
    },
  },
  methods: {
    onEnter(item, index) {
      this.save(item, index);
    },
    onEsc(item) {
      this.errors.clear();

      if (item.is_new) {
        this.items.splice(this.items.length - 1, 1);
      }
      else {
        this.resetItemEditing(item);
      }
    },
    editOn(target_item, index) {
      this.resetEditing();
      target_item.is_editing = true;
      this.setFocus(index);
    },
    add() {
      this.items.push(this.newItem());
      this.setFocus(this.items.length - 1);
    },
    remove(item, index) {
      axios.post(this.data.urls.remove, {
        dictionary: this.data.dictionary,
        id: item.id,
      }).then((response) => {
        if ('error' === response.data.type) {
          this.processErrorsInResponse(response);
        }
        else {
          this.items.splice(index, 1);
          this.processSuccessInResponse(response);
        }
      }).catch((response) => {
        this.processCatch(response);
      });
    },
    resetEditing() {
      for(let i in this.items) {
        this.onEsc(this.items[i]);
        // this.resetItemEditing(this.items[i]);
      }
    },
    setFocus(index) {
      this.$nextTick(() => {
        this.$refs['item_' + index][0].focus();
      });
    },
  },
};
