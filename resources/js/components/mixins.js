export let MsgToUser = {
  methods: {
    showErrors: function(errors) {
      for (let error of errors) {
        this.$toaster.error(error);
      }
    },
    /**
     *
     * @param {string} msg
     */
    showWarning: function(msg) {
      this.$toaster.warning(msg);
    },
    processErrorsInResponse: function(response) {
      this.$toaster.error(response.data.msg);
      if ('undefined' !== typeof response.data.errors) {
        this.showErrors(response.data.errors);
      }
    },
    processSuccessInResponse: function(response) {
      this.$toaster.success(response.data.msg, {timeout: 5000});
      if ('undefined' !== typeof response.data.warning) {
        this.$toaster.warning(response.data.warning);
      }
    },
    processCatch(response) {
      this.$toaster.error(response);
    }
  }
};
