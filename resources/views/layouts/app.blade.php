<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('page_title', config('app.name', 'Laravel'))</title>

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	@stack('styles')
</head>
<body>
<div id="app">
	@spaceless
	<nav class="navbar navbar-expand-lg">
		@component('navbar')@endcomponent
	</nav>
	@endspaceless
	<div class="container">
		<div class="row">
			<div id="content" class="content col-12">
				@yield('content')
			</div>
		</div>
	</div>
</div>

<!-- Scripts -->
@stack('scripts')
</body>
</html>
