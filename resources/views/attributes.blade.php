<?php
/**
 * @var $blocks \App\Attributes\Manage\Block[]
 * @todo: удалить
 */
?>
@extends('layouts.app')
@section('page_title', 'Справочники')

@push('scripts')
  <script src="{{ asset('js/attributes.js') }}"></script>
@endpush

@section('content')
  <h1>Справочники</h1>
  <div class="attributes">
    @if(!empty($product_properties))
      <div class="attribute-block">
        <label>Характеристики</label>
        <ul>
          @foreach($product_properties as $product_property)
            <li>{{ $product_property->title }} ({{ $product_property->type }}): {{ $product_property->value }}</li>
          @endforeach
        </ul>
      </div>
    @endif
  </div>
@endsection


