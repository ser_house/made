<div class="container">
	<a class="navbar-brand" href="{{ route('main') }}">{{ config('app.name', 'Laravel') }}</a>
	<div id="menu">
		<ul class="nav justify-content-end">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('manage_product') }}">Настройка товара</a>
			</li>
		</ul>
	</div>
</div>
