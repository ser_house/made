<?php
/**
 *
 */
?>
@extends('layouts.app')
@section('page_title', 'Настройка товара')

@push('scripts')
  <script src="{{ asset('js/manage_product.js') }}"></script>
@endpush

@section('content')
  <manage-product :tabs='@json($tabs)'></manage-product>
@endsection


