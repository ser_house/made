<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:03
 */


namespace App\Attributes\Manage;


use App\Attributes\Manage\Block;
use Core\Infrastructure\Url\Add;
use Core\Infrastructure\Url\Listing;
use Core\Infrastructure\Url\Remove;
use Core\Infrastructure\Url\Update;

/**
 * Class Manager
 *
 * @package App\Attributes\Manage
 */
class Manager {

  /**
   *
   * @return Block\Block[]
   */
  public function blocks(): array {
    $defaultUrl = new Urls();
    $defaultUrl->setAdd(new Add(route('api_attributes_add')));
    $defaultUrl->setUpdate(new Update(route('api_attributes_update')));
    $defaultUrl->setListing(new Listing(route('api_attributes_listing')));

    // Пока не используется - надо продумать всё про удаление.
    // Например, ед. изм. могут уже использоваться, и тут есть три варианта:
    // 1 удаляем (мягко), больше не даём выбрать для новых объектов, но оставляем у текущих
    // 2 удаляем (мягко) и удаляем все объекты, в которых они задействованы
    // 3 даём удалить только если оно нигде не задействовано
    // При этом удаление справочных данных не имеет особого смысла, собственно:
    // если даже и есть какие-то ненужные, то они совсем не мешают (ибо не много их).
//    $defaultUrl->setRemove(new Remove(route('api_attributes_remove')));

    $blocks = [
      new Block\UnitOfWork($defaultUrl),
      new Block\UnitOfMaterial($defaultUrl),
      new Block\Manufacturer($defaultUrl),
      new Block\Stage($defaultUrl),
      new Block\RoomParam($defaultUrl),
      new Block\ProductProperty($defaultUrl),
      new Block\Category($defaultUrl),
    ];

    usort($blocks, function(Block\Block $left, Block\Block $right) {
      return $left->title() <=> $right->title();
    });

    return $blocks;
  }
}
