<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:01
 */


namespace App\Attributes\Manage;


use Core\Infrastructure\Url\Add;
use Core\Infrastructure\Url\Find;
use Core\Infrastructure\Url\Listing;
use Core\Infrastructure\Url\Remove;
use Core\Infrastructure\Url\Update;

class Urls implements \JsonSerializable {
  /** @var Add */
  public $add;
  /** @var Update */
  public $update;
  /** @var Remove */
  public $remove;
  /** @var Listing */
  public $listing;
  /** @var Find */
  public $find;


  /**
   * @param Add $add
   *
   * @return Urls
   */
  public function setAdd(Add $add): Urls {
    $this->add = $add;

    return $this;
  }

  /**
   * @param Update $update
   *
   * @return Urls
   */
  public function setUpdate(Update $update): Urls {
    $this->update = $update;

    return $this;
  }

  /**
   * @param Remove $remove
   *
   * @return Urls
   */
  public function setRemove(Remove $remove): Urls {
    $this->remove = $remove;

    return $this;
  }

  /**
   * @param Listing $listing
   *
   * @return Urls
   */
  public function setListing(Listing $listing): Urls {
    $this->listing = $listing;

    return $this;
  }

  /**
   * @param Find $find
   *
   * @return Urls
   */
  public function setFind(Find $find): Urls {
    $this->find = $find;

    return $this;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    $json = [];
    if ($this->add) {
      $json['add'] = (string)$this->add;
    }
    if ($this->update) {
      $json['update'] = (string)$this->update;
    }
    if ($this->remove) {
      $json['remove'] = (string)$this->remove;
    }
    if ($this->listing) {
      $json['listing'] = (string)$this->listing;
    }
    if ($this->find) {
      $json['find'] = (string)$this->find;
    }
    return $json;
  }
}
