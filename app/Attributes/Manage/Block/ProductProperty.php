<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:58
 */


namespace App\Attributes\Manage\Block;


use Core\Attribute\Dictionary;
use Core\Attribute\ProductPropertyType;

class ProductProperty extends JustTitled {
  /** @var string */
  protected const DICTIONARY = Dictionary::PRODUCT_PROPERTY;

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    $json = parent::jsonSerialize();
    $json['types'] = ProductPropertyType::titles();
    return $json;
  }
}
