<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:00
 */


namespace App\Attributes\Manage\Block;


use Core\Attribute\Dictionary;
use App\Attributes\Manage\Urls;

abstract class Block implements \JsonSerializable {
  /** @var string */
  protected const DICTIONARY = '';
  /** @var Urls */
  public $urls;

  /**
   * Block constructor.
   *
   * @param Urls $urls
   */
  public function __construct(Urls $urls) {
    $this->urls = $urls;
  }

  public function title(): string {
    return Dictionary::title($this->dictionary());
  }

  public function dictionary(): string {
    return static::DICTIONARY;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'dictionary' => $this->dictionary(),
      'title' => $this->title(),
      'urls' => $this->urls,
    ];
  }
}
