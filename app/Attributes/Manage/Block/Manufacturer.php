<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:58
 */


namespace App\Attributes\Manage\Block;


use Core\Attribute\Dictionary;

class Manufacturer extends JustTitled {
  /** @var string */
  protected const DICTIONARY = Dictionary::MANUFACTURER;
}
