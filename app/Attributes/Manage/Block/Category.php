<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:58
 */


namespace App\Attributes\Manage\Block;


use Core\Attribute\Dictionary;

class Category extends JustTitled {
  /** @var string */
  protected const DICTIONARY = Dictionary::CATEGORY;

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    $json = parent::jsonSerialize();
    $json['max_category_level'] = \App\Model\Category::MAX_LEVEL;
    return $json;
  }
}
