<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 14:34
 */


namespace App\Attributes\Manage\Block;

use Core\Title;

class JustTitled extends Block {

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    $json = parent::jsonSerialize();
    $json['max_title_len'] = Title::MAX_LEN;
    return $json;
  }
}
