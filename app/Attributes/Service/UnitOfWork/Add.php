<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\UnitOfWork;

use App\Attributes\Service\IAddService;
use App\Model\UnitOfWork;
use Core\Attribute\Input\JustTitled\Add as AddInput;
use Core\Infrastructure\View\Item;

class Add implements IAddService {
  /** @var AddInput */
  private $input;

  /**
   * Add constructor.
   *
   * @param AddInput $input
   */
  public function __construct(AddInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = new UnitOfWork(['title' => (string)$this->input->title()]);
    $model->save();

    return Item::buildFromObject($model);
  }
}
