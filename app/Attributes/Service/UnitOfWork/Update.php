<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\UnitOfWork;

use App\Attributes\Service\IUpdateService;
use App\Model\UnitOfWork;
use Core\Attribute\Input\JustTitled\Update as UpdateInput;
use Core\Infrastructure\View\Item;

class Update implements IUpdateService {
  /** @var UpdateInput */
  private $input;

  /**
   * Update constructor.
   *
   * @param UpdateInput $input
   */
  public function __construct(UpdateInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = UnitOfWork::find($this->input->id());
    $model->title = (string)$this->input->title();
    $model->save();

    return Item::buildFromObject($model);
  }
}
