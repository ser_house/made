<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\ProductProperty;

use App\Attributes\Service\IUpdateService;
use App\Model\ProductProperty;
use Core\Attribute\Input\ProductProperty\Update as UpdateInput;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\ProductPropertyItem;

class Update implements IUpdateService {
  /** @var UpdateInput */
  private $input;

  /**
   * Update constructor.
   *
   * @param UpdateInput $input
   */
  public function __construct(UpdateInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = ProductProperty::find($this->input->id());
    if ($this->input->title()) {
      $model->title = (string)$this->input->title();
    }

    if ($this->input->type()) {
      $model->type = $this->input->type();
    }
    $model->save();

    $item = ProductPropertyItem::buildFromObject($model);
    $item->type = $model->type;

    return $item;
  }
}
