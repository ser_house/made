<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\ProductProperty;

use App\Attributes\Service\IAddService;
use App\Model\ProductProperty;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\ProductPropertyItem;
use Core\Attribute\Input\ProductProperty\Add as AddInput;


class Add implements IAddService {
  /** @var AddInput */
  private $input;

  /**
   * Add constructor.
   *
   * @param AddInput $input
   */
  public function __construct(AddInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = new ProductProperty(['title' => (string)$this->input->title(), 'type' => $this->input->type()]);
    $model->save();
    $model = ProductProperty::where(['title' => (string)$this->input->title(), 'type' => $this->input->type()])->first();

    $item = ProductPropertyItem::buildFromObject($model);
    $item->type = $model->type;

    return $item;
  }
}
