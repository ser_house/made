<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\ProductProperty;


use App\Attributes\Service\IListingService;
use App\Model\ProductProperty;
use Core\Attribute\ProductPropertyType;
use Core\Infrastructure\View\ProductPropertyItem;


class All implements IListingService {

  public function run(): array {
    $models = ProductProperty::orderBy('title')->get();
    $items = [];
    foreach ($models as $model) {
      $item = ProductPropertyItem::buildFromObject($model);
      $item->type = $model->type;
      $items[] = $item;
    }

    $types = ProductPropertyType::titles();

    return [
      'items' => $items,
      'types' => $types,
    ];
  }
}
