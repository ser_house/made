<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\ProductProperty;

use App\Attributes\Service\IRemoveService;
use App\Model\ProductProperty;
use Core\Attribute\Input\Remove as RemoveInput;
use Core\Infrastructure\Exception\NotFoundException;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\ProductPropertyItem;


/**
 * Class Remove
 *
 * @package App\Attributes\Service\ProductProperty
 */
class Remove implements IRemoveService {
  /** @var RemoveInput */
  private $input;


  /**
   * Remove constructor.
   *
   * @param RemoveInput $input
   */
  public function __construct(RemoveInput $input) {
    $this->input = $input;
  }

  /**
   * @inheritDoc
   */
  public function run(): Item {
    $model = ProductProperty::find($this->input->id());

    if (null === $model) {
      throw new NotFoundException('Характеристика товара не найдена.');
    }

    $item = ProductPropertyItem::buildFromObject($model);
    $item->type = $model->type;

    $model->delete();

    return $item;
  }
}
