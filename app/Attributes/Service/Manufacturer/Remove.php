<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\Manufacturer;

use App\Attributes\Service\IRemoveService;
use App\Model\Manufacturer;
use Core\Attribute\Input\Remove as RemoveInput;
use Core\Infrastructure\Exception\NotFoundException;
use Core\Infrastructure\View\Item;

/**
 * Class Remove
 *
 * @package App\Attributes\Service\Manufacturer
 */
class Remove implements IRemoveService {
  /** @var RemoveInput */
  private $input;


  /**
   * Remove constructor.
   *
   * @param RemoveInput $input
   */
  public function __construct(RemoveInput $input) {
    $this->input = $input;
  }

  /**
   * @inheritDoc
   */
  public function run(): Item {
    $model = Manufacturer::find($this->input->id());

    if (null === $model) {
      throw new NotFoundException('Производитель не найден.');
    }

    $item = Item::buildFromObject($model);

    $model->delete();

    return $item;
  }
}
