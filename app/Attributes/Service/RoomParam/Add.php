<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\RoomParam;

use App\Attributes\Service\IAddService;
use App\Model\RoomParam;
use App\Model\UnitOfWork;
use Core\Infrastructure\Exception\NotFoundException;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\RoomParamItem;
use Core\Attribute\Input\RoomParam\Add as AddInput;


class Add implements IAddService {
  /** @var AddInput */
  private $input;

  /**
   * Add constructor.
   *
   * @param AddInput $input
   */
  public function __construct(AddInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $unit = UnitOfWork::find($this->input->unitOfWorkId());
    if (empty($unit)) {
      throw new NotFoundException('Ед. изм. не найдена.');
    }

    $params = ['title' => (string)$this->input->title(), 'unit_of_work_id' => $this->input->unitOfWorkId()];

    $model = new RoomParam($params);
    $model->save();
    $model = RoomParam::where($params)->first();

    $item = RoomParamItem::buildFromObject($model);
    $item->unit_of_work_id = (string)$model->unit_of_work_id;

    return $item;
  }
}
