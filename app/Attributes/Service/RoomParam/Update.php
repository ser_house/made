<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\RoomParam;

use App\Attributes\Service\IUpdateService;
use App\Model\RoomParam;
use Core\Attribute\Input\RoomParam\Update as UpdateInput;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\RoomParamItem;

class Update implements IUpdateService {
  /** @var UpdateInput */
  private $input;

  /**
   * Update constructor.
   *
   * @param UpdateInput $input
   */
  public function __construct(UpdateInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = RoomParam::find($this->input->id());
    if ($this->input->title()) {
      $model->title = (string)$this->input->title();
    }

    if ($this->input->unitOfWorkId()) {
      $model->unit_of_work_id = $this->input->unitOfWorkId();
    }
    $model->save();

    $item = RoomParamItem::buildFromObject($model);
    $item->unit_of_work_id = (string)$model->unit_of_work_id;

    return $item;
  }
}
