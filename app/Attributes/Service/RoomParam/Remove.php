<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\RoomParam;

use App\Attributes\Service\IRemoveService;
use App\Model\RoomParam;
use Core\Attribute\Input\Remove as RemoveInput;
use Core\Infrastructure\Exception\NotFoundException;
use Core\Infrastructure\View\Item;
use Core\Infrastructure\View\RoomParamItem;

/**
 * Class Remove
 *
 * @package App\Attributes\Service\RoomParam
 */
class Remove implements IRemoveService {
  /** @var RemoveInput */
  private $input;


  /**
   * Remove constructor.
   *
   * @param RemoveInput $input
   */
  public function __construct(RemoveInput $input) {
    $this->input = $input;
  }

  /**
   * @inheritDoc
   */
  public function run(): Item {
    $model = RoomParam::find($this->input->id());

    if (null === $model) {
      throw new NotFoundException('Параметры не найдены.');
    }

    $item = RoomParamItem::buildFromObject($model);
    $item->unit_of_work_id = (string)$model->unit_of_work_id;

    $model->delete();

    return $item;
  }
}
