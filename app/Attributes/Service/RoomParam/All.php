<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\RoomParam;


use App\Attributes\Service\IListingService;
use App\Model\RoomParam;
use Core\Infrastructure\View\Item;
use App\Model\UnitOfWork;
use Core\Infrastructure\View\RoomParamItem;


class All implements IListingService {

  public function run(): array {
    $models = RoomParam::orderBy('title')->get();
    $items = [];
    foreach ($models as $model) {
      $roomParamItem = RoomParamItem::buildFromObject($model);
      $roomParamItem->unit_of_work_id = (string)$model->unit_of_work_id;
      $items[] = $roomParamItem;
    }

    $unitModels = UnitOfWork::orderBy('title')->get();
    $units = [];
    foreach ($unitModels as $model) {
      $units[] = Item::buildFromObject($model);
    }

    return [
      'items' => $items,
      'units' => $units,
    ];
  }
}
