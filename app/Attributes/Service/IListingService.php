<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:31
 */


namespace App\Attributes\Service;


interface IListingService {

  public function run(): array;
}
