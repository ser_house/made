<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\Category;

use App\Attributes\Service\IAddService;
use App\Model\Category;
use Core\Attribute\Input\Category\Add as AddInput;
use Core\Infrastructure\View\Item;

class Add implements IAddService {
  /** @var AddInput */
  private $input;

  /**
   * Add constructor.
   *
   * @param AddInput $input
   */
  public function __construct(AddInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $params = ['title' => (string)$this->input->title(), 'parent_id' => $this->input->parentId()];

    $model = new Category($params);
    $model->save();
    $model = Category::where($params)->first();

    return Item::buildFromObject($model);
  }
}
