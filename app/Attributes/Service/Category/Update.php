<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\Category;

use App\Attributes\Service\IUpdateService;
use App\Model\Category;
use Core\Attribute\Input\Category\Update as UpdateInput;
use Core\Infrastructure\View\Item;

class Update implements IUpdateService {
  /** @var UpdateInput */
  private $input;

  /**
   * Update constructor.
   *
   * @param UpdateInput $input
   */
  public function __construct(UpdateInput $input) {
    $this->input = $input;
  }

  public function run(): Item {
    $model = Category::find($this->input->id());

    if ($this->input->title()) {
      $model->title = (string)$this->input->title();
    }

    if ($this->input->parentId()) {
      $model->parent_id = $this->input->parentId();
    }

    $model->save();

    return Item::buildFromObject($model);
  }
}
