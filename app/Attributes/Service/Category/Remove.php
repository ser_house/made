<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\Category;

use App\Attributes\Service\IRemoveService;
use App\Model\Category;
use Core\Attribute\Input\Remove as RemoveInput;
use Core\Infrastructure\Exception\NotFoundException;
use Core\Infrastructure\View\Item;

/**
 * Class Remove
 *
 * @package App\Attributes\Service\Category
 */
class Remove implements IRemoveService {
  /** @var RemoveInput */
  private $input;


  /**
   * Remove constructor.
   *
   * @param RemoveInput $input
   */
  public function __construct(RemoveInput $input) {
    $this->input = $input;
  }

  /**
   * @inheritDoc
   */
  public function run(): Item {
    $model = Category::find($this->input->id());

    if (null === $model) {
      throw new NotFoundException('Категория не найдена.');
    }

    $item = Item::buildFromObject($model);

    $model->delete();

    return $item;
  }
}
