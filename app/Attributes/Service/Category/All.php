<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\Category;


use App\Attributes\Service\IListingService;
use App\Model\Category;
use Core\Infrastructure\View\Category as CategoryItem;
use Core\Infrastructure\View\Item;

class All implements IListingService {

  public function run(): array {
    $models = Category::orderBy('title')->get();
    $items = $this->buildTree($models);

    return ['items' => $items];
  }

  /**
   * Строит дерево родитель-потомки.
   *
   * @param iterable $category_models
   *   массив объектов.
   *
   * @return CategoryItem[]
   */
  private function buildTree(iterable $category_models) {
    $items = [];
    foreach($category_models as $model) {
      $item = Item::buildFromObject($model);
      $item->parent_id = $model->parent_id;
      $item->children = [];
      $items[$item->id] = $item;
    }

    $tree = [];
    foreach ($items as $id => $item) {
      $items[$item->parent_id]->children[] = $item;
      if (!$item->parent_id) {
        $tree[] = $item;
      }
    }

    return $tree;
  }
}
