<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 13:16
 */


namespace App\Attributes\Service\UnitOfMaterial;


use App\Attributes\Service\IListingService;
use App\Model\UnitOfMaterial;
use Core\Infrastructure\View\Item;

class All implements IListingService {

  public function run(): array {
    $models = UnitOfMaterial::orderBy('title')->get();
    $items = [];
    foreach ($models as $model) {
      $items[] = Item::buildFromObject($model);
    }

    return ['items' => $items];
  }
}
