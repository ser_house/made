<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:39
 */


namespace App\Attributes\Service;


use App\Attributes\Service\UnitOfMaterial\All as UnitOfMaterialAllService;
use App\Attributes\Service\UnitOfWork\All as UnitOfWorkAllService;
use App\Attributes\Service\Stage\All as StageAllService;
use App\Attributes\Service\Manufacturer\All as ManufacturerAllService;
use App\Attributes\Service\RoomParam\All as RoomParamAllService;
use App\Attributes\Service\ProductProperty\All as ProductPropertyAllService;
use App\Attributes\Service\Category\All as CategoryAllService;

use App\Attributes\Service\UnitOfMaterial\Add as UnitOfMaterialAddService;
use App\Attributes\Service\UnitOfWork\Add as UnitOfWorkAddService;
use App\Attributes\Service\Stage\Add as StageAddService;
use App\Attributes\Service\Manufacturer\Add as ManufacturerAddService;
use App\Attributes\Service\RoomParam\Add as RoomParamAddService;
use App\Attributes\Service\ProductProperty\Add as ProductPropertyAddService;
use App\Attributes\Service\Category\Add as CategoryAddService;

use App\Attributes\Service\UnitOfMaterial\Update as UnitOfMaterialUpdateService;
use App\Attributes\Service\UnitOfWork\Update as UnitOfWorkUpdateService;
use App\Attributes\Service\Stage\Update as StageUpdateService;
use App\Attributes\Service\Manufacturer\Update as ManufacturerUpdateService;
use App\Attributes\Service\RoomParam\Update as RoomParamUpdateService;
use App\Attributes\Service\ProductProperty\Update as ProductPropertyUpdateService;
use App\Attributes\Service\Category\Update as CategoryUpdateService;

use App\Attributes\Service\UnitOfMaterial\Remove as UnitOfMaterialRemoveService;
use App\Attributes\Service\UnitOfWork\Remove as UnitOfWorkRemoveService;
use App\Attributes\Service\Stage\Remove as StageRemoveService;
use App\Attributes\Service\Manufacturer\Remove as ManufacturerRemoveService;
use App\Attributes\Service\RoomParam\Remove as RoomParamRemoveService;
use App\Attributes\Service\ProductProperty\Remove as ProductPropertyRemoveService;
use App\Attributes\Service\Category\Remove as CategoryRemoveService;

use Core\Attribute\Input\JustTitled\Add as JustTitledAddInput;
use Core\Attribute\Input\RoomParam\Add as RoomParamAddInput;
use Core\Attribute\Input\ProductProperty\Add as ProductPropertyAddInput;
use Core\Attribute\Input\Category\Add as CategoryAddInput;

use Core\Attribute\Input\JustTitled\Update as JustTitledUpdateInput;
use Core\Attribute\Input\RoomParam\Update as RoomParamUpdateInput;
use Core\Attribute\Input\ProductProperty\Update as ProductPropertyUpdateInput;
use Core\Attribute\Input\Category\Update as CategoryUpdateInput;

use Core\Attribute\Input\Remove as RemoveInput;


use Core\Attribute\Dictionary;


/**
 * Class Factory
 *
 * @package App\Attributes\Service
 */
class Factory {

  /**
   * @param string $dictionary
   *
   * @return IListingService
   * @throws \Exception
   */
  public function getListingService(string $dictionary): IListingService {
    switch ($dictionary) {
      case Dictionary::UNIT_OF_MATERIAL:
        return new UnitOfMaterialAllService();

      case Dictionary::UNIT_OF_WORK:
        return new UnitOfWorkAllService();

      case Dictionary::MANUFACTURER:
        return new ManufacturerAllService();

      case Dictionary::STAGE:
        return new StageAllService();

      case Dictionary::ROOM_PARAM:
        return new RoomParamAllService();

      case Dictionary::PRODUCT_PROPERTY:
        return new ProductPropertyAllService();

      case Dictionary::CATEGORY:
        return new CategoryAllService();

      default:
        throw new \Exception("Сервис листинга для словаря '$dictionary' не найден.");
    }
  }

  /**
   * @param string $dictionary
   * @param array $raw_input
   *
   * @return IAddService
   * @throws \Core\Infrastructure\Exception\MissingParamException
   */
  public function getAddService(string $dictionary, array $raw_input): IAddService {
    switch ($dictionary) {
      case Dictionary::UNIT_OF_MATERIAL:
        $input = new JustTitledAddInput($raw_input);
        return new UnitOfMaterialAddService($input);

      case Dictionary::UNIT_OF_WORK:
        $input = new JustTitledAddInput($raw_input);
        return new UnitOfWorkAddService($input);

      case Dictionary::MANUFACTURER:
        $input = new JustTitledAddInput($raw_input);
        return new ManufacturerAddService($input);

      case Dictionary::STAGE:
        $input = new JustTitledAddInput($raw_input);
        return new StageAddService($input);

      case Dictionary::ROOM_PARAM:
        $input = new RoomParamAddInput($raw_input);
        return new RoomParamAddService($input);

      case Dictionary::PRODUCT_PROPERTY:
        $input = new ProductPropertyAddInput($raw_input);
        return new ProductPropertyAddService($input);

      case Dictionary::CATEGORY:
        $input = new CategoryAddInput($raw_input);
        return new CategoryAddService($input);

      default:
        throw new \Exception("Сервис добавления для словаря '$dictionary' не найден.");
    }
  }

  /**
   * @param string $dictionary
   * @param array $raw_input
   *
   * @return IUpdateService
   * @throws \Core\Infrastructure\Exception\MissingParamException
   */
  public function getUpdateService(string $dictionary, array $raw_input): IUpdateService {
    switch ($dictionary) {
      case Dictionary::UNIT_OF_MATERIAL:
        $input = new JustTitledUpdateInput($raw_input);
        return new UnitOfMaterialUpdateService($input);

      case Dictionary::UNIT_OF_WORK:
        $input = new JustTitledUpdateInput($raw_input);
        return new UnitOfWorkUpdateService($input);

      case Dictionary::MANUFACTURER:
        $input = new JustTitledUpdateInput($raw_input);
        return new ManufacturerUpdateService($input);

      case Dictionary::STAGE:
        $input = new JustTitledUpdateInput($raw_input);
        return new StageUpdateService($input);

      case Dictionary::ROOM_PARAM:
        $input = new RoomParamUpdateInput($raw_input);
        return new RoomParamUpdateService($input);

      case Dictionary::PRODUCT_PROPERTY:
        $input = new ProductPropertyUpdateInput($raw_input);
        return new ProductPropertyUpdateService($input);

      case Dictionary::CATEGORY:
        $input = new CategoryUpdateInput($raw_input);
        return new CategoryUpdateService($input);

      default:
        throw new \Exception("Сервис обновления для словаря '$dictionary' не найден.");
    }
  }

  /**
   * @param string $dictionary
   * @param array $raw_input
   *
   * @return IRemoveService
   * @throws \Core\Infrastructure\Exception\MissingParamException
   */
  public function getRemoveService(string $dictionary, array $raw_input): IRemoveService {
    switch ($dictionary) {
      case Dictionary::UNIT_OF_MATERIAL:
        $input = new RemoveInput($raw_input);
        return new UnitOfMaterialRemoveService($input);

      case Dictionary::UNIT_OF_WORK:
        $input = new RemoveInput($raw_input);
        return new UnitOfWorkRemoveService($input);

      case Dictionary::MANUFACTURER:
        $input = new RemoveInput($raw_input);
        return new ManufacturerRemoveService($input);

      case Dictionary::STAGE:
        $input = new RemoveInput($raw_input);
        return new StageRemoveService($input);

      case Dictionary::ROOM_PARAM:
        $input = new RemoveInput($raw_input);
        return new RoomParamRemoveService($input);

      case Dictionary::PRODUCT_PROPERTY:
        $input = new RemoveInput($raw_input);
        return new ProductPropertyRemoveService($input);

      case Dictionary::CATEGORY:
        $input = new RemoveInput($raw_input);
        return new CategoryRemoveService($input);

      default:
        throw new \Exception("Сервис удаления для словаря '$dictionary' не найден.");
    }
  }
}
