<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2020
 * Time: 16:59
 */


namespace App\Http\Controllers;


class Main extends Controller {
  public function __invoke() {
    return view('main', ['products' => []]);
  }
}
