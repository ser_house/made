<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 9:21
 */


namespace App\Http\Controllers\Api;


use App\Attributes\Service\Factory;
use App\Http\Requests\ApiAttributeRequest;


/**
 * Class Attributes
 *
 * @package App\Http\Controllers\Api
 */
class Attributes {
  /** @var Factory */
  private $serviceFactory;

  /**
   * Attributes constructor.
   *
   * @param Factory $serviceFactory
   */
  public function __construct(Factory $serviceFactory) {
    $this->serviceFactory = $serviceFactory;
  }

  /**
   * @param ApiAttributeRequest $request
   */
  public function find(ApiAttributeRequest $request) {

  }

  /**
   * @param ApiAttributeRequest $request
   *
   * @return \Illuminate\Http\JsonResponse
   * @throws \Exception
   */
  public function listing(ApiAttributeRequest $request) {
    $validated = $request->validated();

    $service = $this->serviceFactory->getListingService($validated['dictionary']);
    $data = $service->run();

    $data['type'] = 'success';

    return response()->json($data);
  }

  /**
   * @param ApiAttributeRequest $request
   *
   * @return \Illuminate\Http\JsonResponse
   * @throws \Core\Infrastructure\Exception\MissingParamException
   */
  public function add(ApiAttributeRequest $request) {
    $validated = $request->validated();

    $service = $this->serviceFactory->getAddService($validated['dictionary'], $request->all());
    $item = $service->run();

    $result = [
      'type' => 'success',
      'msg' => 'Добавлено.',
      'item' => $item,
    ];
    return response()->json($result, 201);
  }

  /**
   * @param ApiAttributeRequest $request
   *
   * @return \Illuminate\Http\JsonResponse
   * @throws \Core\Infrastructure\Exception\MissingParamException
   */
  public function update(ApiAttributeRequest $request) {
    $validated = $request->validated();

    $service = $this->serviceFactory->getUpdateService($validated['dictionary'], $request->all());
    $item = $service->run();

    return response()->json([
      'type' => 'success',
      'msg' => 'Обновлено.',
      'item' => $item,
    ]);
  }

  /**
   * @param ApiAttributeRequest $request
   *
   * @return \Illuminate\Http\JsonResponse
   * @throws \Core\Infrastructure\Exception\MissingParamException
   * @throws \Core\Infrastructure\Exception\NotFoundException
   */
  public function remove(ApiAttributeRequest $request) {
    $validated = $request->validated();

    $service = $this->serviceFactory->getRemoveService($validated['dictionary'], $request->all());
    $item = $service->run();

    return response()->json([
      'type' => 'success',
      'msg' => 'Удалено.',
      'item' => $item,
    ]);
  }
}
