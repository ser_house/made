<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 17:01
 */


namespace App\Http\Controllers\Manage;


use App\Attributes\Manage\Manager as AttributesManager;
use App\Http\Controllers\Controller;

class Product extends Controller {
  /**
   * @inheritDoc
   */
  public function __invoke(AttributesManager $manager) {
    return view('manage.product', [
      'tabs' => [
        'attributes' => [
          'content' => $manager->blocks(),
          'title' => 'Справочники',
        ],
        'works' => [
          'content' => [],
          'title' => 'Работы',
        ],
        'products' => [
          'content' => [],
          'title' => 'Товары',
        ],
        'prices' => [
          'content' => [],
          'title' => 'Цены',
        ],
      ],
    ]);
  }


}
