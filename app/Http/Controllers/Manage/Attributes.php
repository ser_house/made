<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.02.2020
 * Time: 16:59
 */


namespace App\Http\Controllers\Manage;


use App\Http\Controllers\Controller;
use App\Model\Manufacturer;
use App\Model\ProductProperty;
use App\Model\RoomParam;
use App\Model\Stage;
use App\Model\UnitOfMaterial;
use App\Model\UnitOfWork;
use App\Attributes\Manage\Manager;

// @todo: удалить
class Attributes extends Controller {
  public function __invoke(Manager $manager) {
    $units_of_work = UnitOfWork::all();
    $units_of_material = UnitOfMaterial::all();
    $stages = Stage::all();
    $manufacturers = Manufacturer::all();
    $product_properties = ProductProperty::all();
    $room_params = RoomParam::with('unitOfWork')->get();

    return view('manage.product', [
      'blocks' => $manager->blocks(),
    ]);
  }
}
