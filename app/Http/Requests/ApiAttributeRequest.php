<?php

namespace App\Http\Requests;

use Core\Attribute\Dictionary;
use Illuminate\Foundation\Http\FormRequest;

class ApiAttributeRequest extends FormRequest {
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules() {
    $dictionaries = array_keys(Dictionary::titles());
    $dictionaries_str = implode(',', $dictionaries);

    return [
      'dictionary' => "required|in:$dictionaries_str",
    ];
  }
}
