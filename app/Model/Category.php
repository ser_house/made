<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Category
 *
 * @property string $id
 * @property string $parent_id
 * @property string $deleted_at
 * @property string $title
 * @property Category $category
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @method static Builder|Category whereDeletedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereParentId($value)
 * @method static Builder|Category whereTitle($value)
 * @mixin Eloquent
 */
class Category extends Model {
  use SoftDeletes;

  public const MAX_LEVEL = 7;

  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'category';

  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;

  public $timestamps = false;

  /**
   * @var array
   */
  protected $fillable = ['parent_id', 'deleted_at', 'title'];

  /**
   * @return BelongsTo
   */
  public function category() {
    return $this->belongsTo('App\Model\Category', 'parent_id');
  }
}
