<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Manufacturer
 *
 * @property int $id
 * @property string $deleted_at
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Manufacturer whereTitle($value)
 * @mixin \Eloquent
 */
class Manufacturer extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'manufacturer';

  public $timestamps = false;

  /**
   * @var array
   */
  protected $fillable = ['deleted_at', 'title'];

}
