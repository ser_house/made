<?php

namespace App\Model;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\RoomParam
 *
 * @property string $id
 * @property integer $unit_of_work_id
 * @property string $deleted_at
 * @property string $title
 * @property UnitOfWork $unitOfWork
 * @method static Builder|RoomParam newModelQuery()
 * @method static Builder|RoomParam newQuery()
 * @method static Builder|RoomParam query()
 * @method static Builder|RoomParam whereDeletedAt($value)
 * @method static Builder|RoomParam whereId($value)
 * @method static Builder|RoomParam whereTitle($value)
 * @method static Builder|RoomParam whereUnitOfWorkId($value)
 * @mixin Eloquent
 */
class RoomParam extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'room_param';

  public $timestamps = false;

  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;

  /**
   * @var array
   */
  protected $fillable = ['unit_of_work_id', 'deleted_at', 'title'];

  /**
   * @return BelongsTo
   */
  public function unitOfWork() {
    return $this->belongsTo('App\Model\UnitOfWork');
  }
}
