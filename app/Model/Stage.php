<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Stage
 *
 * @property int $id
 * @property string $deleted_at
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Stage whereTitle($value)
 * @mixin \Eloquent
 */
class Stage extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'stage';

  public $timestamps = false;

  /**
   * @var array
   */
  protected $fillable = ['deleted_at', 'title'];

}
