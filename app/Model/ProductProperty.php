<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\ProductProperty
 *
 * @property string $id
 * @property string $deleted_at
 * @property string $title
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\ProductProperty whereType($value)
 * @mixin \Eloquent
 */
class ProductProperty extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_property';

  /**
   * @var string
   */
  protected $keyType = 'string';

  public $timestamps = false;

  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;

  /**
   * @var array
   */
  protected $fillable = ['deleted_at', 'title', 'type'];

}
