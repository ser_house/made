<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\UnitOfWork
 *
 * @property int $id
 * @property string $deleted_at
 * @property string $title
 * @property RoomParam[] $roomParams
 * @property-read int|null $room_params_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfWork whereTitle($value)
 * @mixin \Eloquent
 */
class UnitOfWork extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'unit_of_work';

  public $timestamps = false;

  /**
   * @var array
   */
  protected $fillable = ['deleted_at', 'title'];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function roomParams() {
    return $this->hasMany('App\Model\RoomParam');
  }
}
