<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\UnitOfMaterial
 *
 * @property int $id
 * @property string $deleted_at
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\UnitOfMaterial whereTitle($value)
 * @mixin \Eloquent
 */
class UnitOfMaterial extends Model {
  use SoftDeletes;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'unit_of_material';

  public $timestamps = false;

  /**
   * @var array
   */
  protected $fillable = ['deleted_at', 'title'];

}
