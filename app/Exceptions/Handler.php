<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {
  /**
   * A list of the exception types that are not reported.
   *
   * @var array
   */
  protected $dontReport = [
    //
  ];

  /**
   * A list of the inputs that are never flashed for validation exceptions.
   *
   * @var array
   */
  protected $dontFlash = [
    'password',
    'password_confirmation',
  ];

  /**
   * Report or log an exception.
   *
   * @param \Exception $exception
   *
   * @return void
   *
   * @throws \Exception
   */
  public function report(Exception $exception) {
    parent::report($exception);
  }

  /**
   * Render an exception into an HTTP response.
   *
   * @param \Illuminate\Http\Request $request
   * @param \Exception $exception
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *
   * @throws \Exception
   */
  public function render($request, Exception $exception) {
    if ($request->is('api/*')) {
      return $this->renderApiException($request, $exception);
    }

    return parent::render($request, $exception);
  }

  /**
   * @param $request
   * @param Exception $exception
   *
   * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
   */
  private function renderApiException($request, Exception $exception) {
    $msg = $exception->getMessage();

    $code = 400;
    if ($exception instanceof QueryException) {
      // просто чтобы пользователю не вываливались внутреняя информация
      if (false !== strpos($msg, 'Unique violation')) {
        $msg = 'Такое значение уже есть.';
      }
      else {
        $msg = 'Операция невозможна.';
        $code = 500;
      }
    }

    return response()->json([
      'type' => 'error',
      'msg' => $msg,
    ], $code);
  }
}
