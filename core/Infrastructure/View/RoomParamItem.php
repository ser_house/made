<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 17:31
 */


namespace Core\Infrastructure\View;


class RoomParamItem extends Item {
  /** @var string */
  public $unit_of_work_id;
}
