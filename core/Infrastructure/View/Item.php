<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 11:00
 */


namespace Core\Infrastructure\View;


class Item {
  /** @var string */
  public $id;
  /** @var string */
  public $title;

  /**
   * Item constructor.
   *
   * @param string $id
   * @param string $title
   */
  public function __construct(string $id, string $title) {
    $this->id = $id;
    $this->title = $title;
  }

  public static function buildFromObject($object): self {
    return new self($object->id, $object->title);
  }
}
