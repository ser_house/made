<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 21:07
 */


namespace Core\Infrastructure\View;


class Category extends Item {
  /** @var Item[] */
  public $children;
}
