<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 16:01
 */


namespace Core\Infrastructure\Exception;


class NotFoundException extends \Exception {

}
