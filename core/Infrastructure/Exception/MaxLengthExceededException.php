<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 14:30
 */


namespace Core\Infrastructure\Exception;


class MaxLengthExceededException extends \InvalidArgumentException {

}
