<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 15:43
 */


namespace Core\Infrastructure\Exception;


class MissingParamException extends \Exception {

}
