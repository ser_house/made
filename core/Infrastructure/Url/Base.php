<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 8:08
 */


namespace Core\Infrastructure\Url;


abstract class Base {
  /** @var string */
  protected $value;

  /**
   * Base constructor.
   *
   * @param string $value
   */
  public function __construct(string $value) {
    $this->value = $value;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }
}
