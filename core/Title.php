<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.02.2020
 * Time: 23:19
 */


namespace Core;


use Core\Infrastructure\Exception\MaxLengthExceededException;

class Title {

  /** @var int максимально допустимая длина названия */
  public const MAX_LEN = 100;

  /** @var string $value */
  protected $value;

  /**
   * Title constructor.
   *
   * @param string $value
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(string $value) {
    $value = trim($value);
    if (empty($value)) {
      throw new \InvalidArgumentException("Недостаточная длина названия: '$value'");
    }

    if (mb_strlen($value) > static::MAX_LEN) {
      throw new MaxLengthExceededException('Превышена максимальная длина названия (' . static::MAX_LEN . ')');
    }

    $this->value = $value;
  }

  /**
   * @return string
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * @param Title $title
   *
   * @return bool
   */
  public function equals(Title $title): bool {
    return $title->value() === $this->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }
}
