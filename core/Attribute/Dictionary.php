<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.02.2020
 * Time: 10:40
 */


namespace Core\Attribute;


final class Dictionary {

  public const MANUFACTURER = 'manufacturer';
  public const UNIT_OF_MATERIAL = 'unit_of_material';
  public const UNIT_OF_WORK = 'unit_of_work';
  public const STAGE = 'stage';
  public const ROOM_PARAM = 'room_param';
  public const PRODUCT_PROPERTY = 'product_property';
  public const CATEGORY = 'category';

  public static function titles(): array {
    return [
      self::MANUFACTURER => 'Производители',
      self::UNIT_OF_MATERIAL => 'Единицы измерения (Материалы)',
      self::UNIT_OF_WORK => 'Единицы измерения (Работы)',
      self::STAGE => 'Этапы',
      self::ROOM_PARAM => 'Параметры объекта',
      self::PRODUCT_PROPERTY => 'Характеристики товаров',
      self::CATEGORY => 'Категории',
    ];
  }

  public static function title(string $type): string {
    $titles = self::titles();

    if (!isset($titles[$type])) {
      throw new \InvalidArgumentException("Тип справочника '$type' не определен.");
    }

    return $titles[$type];
  }
}
