<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 19:55
 */


namespace Core\Attribute\Input;


use Core\Infrastructure\Exception\MissingParamException;

class Remove {
  /** @var string */
  private $id;

  /**
   * Remove constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['id'])) {
      throw new MissingParamException('Не указан обязательный параметр "id".');
    }

    $this->id = $raw_input['id'];
  }

  /**
   * @return string
   */
  public function id(): string {
    return $this->id;
  }
}
