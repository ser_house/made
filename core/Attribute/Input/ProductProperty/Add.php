<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:18
 */


namespace Core\Attribute\Input\ProductProperty;

use Core\Attribute\ProductPropertyType;
use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

/**
 * Class Add
 *
 * @package Core\Attribute\Input\ProductProperty
 */
class Add {
  /** @var Title */
  private $title;
  /** @var string */
  private $type;


  /**
   * Add constructor.
   *
   * @param array $raw_input
   *
   * @throws MissingParamException
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['title'])) {
      throw new MissingParamException('Не указан обязательный параметр "title".');
    }
    if (empty($raw_input['type'])) {
      throw new MissingParamException('Не указан обязательный параметр "type".');
    }

    // Проверка на корректный тип.
    ProductPropertyType::title($raw_input['type']);

    $this->title = new Title($raw_input['title']);
    $this->type = $raw_input['type'];
  }

  /**
   * @return Title
   */
  public function title(): Title {
    return $this->title;
  }

  /**
   * @return string
   */
  public function type(): string {
    return $this->type;
  }
}
