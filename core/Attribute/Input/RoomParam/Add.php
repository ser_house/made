<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:18
 */


namespace Core\Attribute\Input\RoomParam;

use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

class Add {
  /** @var int */
  private $unit_of_work_id;
  /** @var Title */
  private $title;

  /**
   * Add constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['unit_of_work_id'])) {
      throw new MissingParamException('Не указан обязательный параметр "unit_of_work_id".');
    }
    if (empty($raw_input['title'])) {
      throw new MissingParamException('Не указан обязательный параметр "title".');
    }

    $this->title = new Title($raw_input['title']);
    $this->unit_of_work_id = $raw_input['unit_of_work_id'];
  }

  /**
   * @return Title
   */
  public function title(): Title {
    return $this->title;
  }

  /**
   * @return int
   */
  public function unitOfWorkId(): int {
    return $this->unit_of_work_id;
  }
}
