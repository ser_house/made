<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:19
 */


namespace Core\Attribute\Input\RoomParam;

use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

class Update {
  /** @var string */
  private $id;
  /** @var Title|null */
  private $title;
  /** @var string|null */
  private $unit_of_work_id;


  /**
   * Update constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['id'])) {
      throw new MissingParamException('Не указан обязательный параметр "id".');
    }

    if (empty($raw_input['title']) && empty($raw_input['unit_of_work_id'])) {
      throw new MissingParamException('Не указан обновляемый параметр "title" или "unit_of_work_id".');
    }

    $this->id = $raw_input['id'];

    if (!empty($raw_input['title'])) {
      $this->title = new Title($raw_input['title']);
    }

    if (!empty($raw_input['unit_of_work_id'])) {
      $this->unit_of_work_id = (string)$raw_input['unit_of_work_id'];
    }
  }

  /**
   * @return string
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * @return Title|null
   */
  public function title(): ?Title {
    return $this->title;
  }

  /**
   * @return string|null
   */
  public function unitOfWorkId(): ?string {
    return $this->unit_of_work_id;
  }
}
