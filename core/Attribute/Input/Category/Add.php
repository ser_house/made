<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:18
 */


namespace Core\Attribute\Input\Category;

use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

class Add {
  /** @var string|null */
  private $parent_id;
  /** @var Title */
  private $title;

  /**
   * Add constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['title'])) {
      throw new MissingParamException('Не указан обязательный параметр "title".');
    }

    $this->title = new Title($raw_input['title']);
    $this->parent_id = $raw_input['parent_id'] ?? null;
  }

  /**
   * @return Title
   */
  public function title(): Title {
    return $this->title;
  }

  /**
   * @return string|null
   */
  public function parentId(): ?string {
    return $this->parent_id;
  }
}
