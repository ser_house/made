<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:19
 */


namespace Core\Attribute\Input\Category;

use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

class Update {
  /** @var string */
  private $id;
  /** @var Title|null */
  private $title;
  /** @var string|null */
  private $parent_id;


  /**
   * Update constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['id'])) {
      throw new MissingParamException('Не указан обязательный параметр "id".');
    }

    if (empty($raw_input['title']) && empty($raw_input['parent_id'])) {
      throw new MissingParamException('Не указан обновляемый параметр "title" или "parent_id".');
    }

    $this->id = $raw_input['id'];

    if (!empty($raw_input['title'])) {
      $this->title = new Title($raw_input['title']);
    }

    if (!empty($raw_input['parent_id'])) {
      $this->parent_id = (string)$raw_input['parent_id'];
    }
  }

  /**
   * @return string
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * @return Title|null
   */
  public function title(): ?Title {
    return $this->title;
  }

  /**
   * @return string|null
   */
  public function parentId(): ?string {
    return $this->parent_id;
  }
}
