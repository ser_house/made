<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:18
 */


namespace Core\Attribute\Input\JustTitled;

use Core\Infrastructure\Exception\MaxLengthExceededException;
use Core\Infrastructure\Exception\MissingParamException;
use Core\Title;

/**
 * Class Add
 *
 * @package Core\Attribute\Input\JustTitled
 */
class Add {

  /** @var Title */
  protected $title;


  /**
   * Add constructor.
   *
   * @param array $raw_input
   *
   * @throws MissingParamException
   * @throws MaxLengthExceededException
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['title'])) {
      throw new MissingParamException('Не указан обязательный параметр "title".');
    }

    $this->title = new Title($raw_input['title']);
  }

  /**
   * @return Title
   */
  public function title(): Title {
    return $this->title;
  }
}
