<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 12:19
 */


namespace Core\Attribute\Input\JustTitled;

use Core\Infrastructure\Exception\MissingParamException;

class Update extends Add {
  /** @var int */
  private $id;


  /**
   * Update constructor.
   *
   * @param array $raw_input
   */
  public function __construct(array $raw_input) {
    if (empty($raw_input['id'])) {
      throw new MissingParamException('Не указан обязательный параметр "id".');
    }

    parent::__construct($raw_input);

    $this->id = $raw_input['id'];
  }


  /**
   * @return int
   */
  public function id(): int {
    return $this->id;
  }
}
