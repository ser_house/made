<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 12.02.2020
 * Time: 16:08
 */


namespace Core\Attribute;


final class ProductPropertyType {

  public const DECIMAL = 'decimal';
  public const INTEGER = 'integer';
  public const STRING = 'string';
  public const REFERENCE = 'reference';
  public const OPTIONS = 'options';

  public static function titles(): array {
    return [
      self::DECIMAL => 'Число',
      self::INTEGER => 'Целое число',
      self::STRING => 'Строка',
      self::REFERENCE => 'Связь со справочником',
      self::OPTIONS => 'Опции',
    ];
  }

  public static function title(string $type): string {
    $titles = self::titles();

    if (!isset($titles[$type])) {
      throw new \InvalidArgumentException("Тип характеристики товара '$type' не определен.");
    }

    return $titles[$type];
  }
}
