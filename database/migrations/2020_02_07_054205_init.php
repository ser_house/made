<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Init extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    $simpleDefinition = function (Blueprint $table) {
      $table->increments('id');
      $table->softDeletes();
      $table->string('title');

      $table->unique('title');
    };

    Schema::create('unit_of_work', $simpleDefinition);
    Schema::create('unit_of_material', $simpleDefinition);
    Schema::create('stage', $simpleDefinition);
    Schema::create('manufacturer', $simpleDefinition);


    DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');

    Schema::create('room_param', function (Blueprint $table) {
      $table->uuid('id')->primary()->default(DB::raw('uuid_generate_v4()'));
      $table->softDeletes();
      $table->string('title');
      $table->unsignedInteger('unit_of_work_id');

      $table->unique(['title', 'unit_of_work_id']);

      $table->foreign('unit_of_work_id')
        ->references('id')->on('unit_of_work')->onDelete('cascade')->onUpdate('restrict');
    });


    $data_types = [
      'decimal',
      'integer',
      'string',
      'reference',
      'options',
    ];
    Schema::create('product_property', function (Blueprint $table) use ($data_types) {
      $table->uuid('id')->primary()->default(DB::raw('uuid_generate_v4()'));
      $table->softDeletes();
      $table->string('title');
      $table->enum('type', $data_types);
      $table->unique(['title', 'type']);
    });

    Schema::create('category', function (Blueprint $table) {
      $table->uuid('id')->primary()->default(DB::raw('uuid_generate_v4()'));
      $table->softDeletes();
      $table->string('title');
      $table->uuid('parent_id')->nullable();

      $table->unique('id');
      $table->unique('title');

      $table->foreign('parent_id')
        ->references('id')->on('category')->onDelete('cascade')->onUpdate('restrict');
    });

    $this->fillData();
  }

  private function fillData() {
    $units_of_material = [
      ['title' => 'м2'],
      ['title' => 'мп'],
      ['title' => 'шт'],
      ['title' => 'упаковка'],
    ];

    DB::table('unit_of_material')->insert($units_of_material);

    $units_of_work = [
      ['title' => 'м2'],
      ['title' => 'мп'],
      ['title' => 'шт'],
      ['title' => 'комплекс'],
      ['title' => 'час'],
      ['title' => 'выезд'],
    ];

    DB::table('unit_of_work')->insert($units_of_work);

    $stages = [
      ['title' => '0: Демонтаж'],
      ['title' => '1: Проектирование'],
      ['title' => '2: Черновые работы'],
      ['title' => '3: Сантехника, электрика, стяжка'],
      ['title' => '4: Чистовые работы'],
    ];
    DB::table('stage')->insert($stages);

    $manufacturers = [
      ['title' => 'Knauf'],
      ['title' => 'Weber-vetonit'],
      ['title' => 'Tarkett'],
      ['title' => 'Grohe'],
      ['title' => 'Ideal Standart'],
    ];
    DB::table('manufacturer')->insert($manufacturers);

    $units_of_work = DB::select('SELECT * FROM unit_of_work');
    $unit_ids = [];
    foreach($units_of_work as $unit_of_work) {
      $unit_ids[$unit_of_work->title] = $unit_of_work->id;
    }
    $room_params = [
      'Площадь пола' => $unit_ids['м2'],
      'Периметр пола' => $unit_ids['мп'],
      'Периметр пола за вычетом проемов' => $unit_ids['мп'],
      'Площадь стен' => $unit_ids['м2'],
      'Периметр дверных откосов' => $unit_ids['мп'],
      'Периметр оконных откосов' => $unit_ids['мп'],
      'Площадь оконных откосов' => $unit_ids['м2'],
      'Периметр потолка' => $unit_ids['мп'],
      'Площадь потолка' => $unit_ids['м2'],
    ];
    foreach($room_params as $title => $unit_id) {
      DB::table('room_param')->insert(['title' => $title, 'unit_of_work_id' => $unit_id]);
    }

    $data = [
      'Объем, м3' => 'decimal',
      'Вес, кг' => 'decimal',
      'Производитель' => 'reference',
      'Гарантия' => 'string',
      'Длина, мм' => 'integer',
      'Высота, мм' => 'integer',
      'Кол-во штук в упаковке' => 'integer',
      'Цвет' => 'options',
    ];
    foreach($data as $title => $type) {
      DB::table('product_property')->insert([
        'title' => $title,
        'type' => $type,
      ]);
    }

    $categories = [
      'Черновые материалы' => '0fea7b1e-da0e-4d9f-a2e0-fcb2555e84ed',
      'Чистовые материалы' => '4ff4a6af-c0d9-4836-8a7e-4c2333e2e45a',
      'Мебель' => '0bf6993f-a9ee-4015-acdd-9c3bdb301387',
      'Свет' => 'd87611de-e17d-462f-b61d-c0050a6568d9',
    ];
    foreach($categories as $title => $id) {
      DB::table('category')->insert(['id' => $id, 'title' => $title]);
    }
    $categories2 = [
      'Смеси' => [
        'id' => 'cde65595-0f88-4222-b985-e87ae4cee5ef',
        'title' => 'Смеси',
        'parent_id' => $categories['Черновые материалы'],
      ],
      'Конструкции' => [
        'id' => 'a7f58517-d46f-4527-ac97-d158713717d6',
        'title' => 'Конструкции',
        'parent_id' => $categories['Черновые материалы'],
      ],
      'Ламинат' => [
        'id' => '4d6134df-6ce4-42d3-8af5-f5c3bc9168fe',
        'title' => 'Ламинат',
        'parent_id' => $categories['Чистовые материалы'],
      ],
      'Полотенцесушители' => [
        'id' => '9c1f7406-bbe6-4228-850a-5299b89b960f',
        'title' => 'Полотенцесушители',
        'parent_id' => $categories['Чистовые материалы'],
      ],
    ];
    DB::table('category')->insert($categories2);
    $categories3 = [
      'Штукатурка' => [
        'id' => 'bc1e487d-c0e3-45ba-aba1-0ee43c67f832',
        'title' => 'Штукатурка',
        'parent_id' => $categories2['Смеси']['id'],
      ],
      'Грунтовка' => [
        'id' => '668aa087-b99d-47c2-8071-c59fd262eaa5',
        'title' => 'Грунтовка',
        'parent_id' => $categories2['Смеси']['id'],
      ],
      'Шпатлевка' => [
        'id' => 'f40aec3b-b908-4542-9f55-7cad853784d4',
        'title' => 'Шпатлевка',
        'parent_id' => $categories2['Смеси']['id'],
      ],
      'Обработка' => [
        'id' => '674a37e9-79b0-4487-8086-ab4ee341f96c',
        'title' => 'Обработка',
        'parent_id' => $categories2['Конструкции']['id'],
      ],
      'Усиление' => [
        'id' => '0d336df9-9f8b-4b97-9116-7489a452271c',
        'title' => 'Усиление',
        'parent_id' => $categories2['Конструкции']['id'],
      ],
      'Подготовка' => [
        'id' => '72cb7c0b-ada1-4691-9a60-91bdadaf2608',
        'title' => 'Подготовка',
        'parent_id' => $categories2['Конструкции']['id'],
      ],
      'Лента' => [
        'id' => 'e416754d-0b77-4fba-9684-a4f97e002e4d',
        'title' => 'Лента',
        'parent_id' => $categories2['Конструкции']['id'],
      ],
      'Крепеж' => [
        'id' => 'a95c95bc-3baf-413d-a586-eb702285377e',
        'title' => 'Крепеж',
        'parent_id' => $categories2['Конструкции']['id'],
      ],
      'Полотенцесушитель водяной' => [
        'id' => '392c5323-8caa-4d82-8e2a-cfab3806332b',
        'title' => 'Полотенцесушитель водяной',
        'parent_id' => $categories2['Полотенцесушители']['id'],
      ],
      'Полотенцесушитель электрический' => [
        'id' => '604daff1-1bdb-42b9-94e0-ba6cbbc6650a',
        'title' => 'Полотенцесушитель электрический',
        'parent_id' => $categories2['Полотенцесушители']['id'],
      ],
    ];
    DB::table('category')->insert($categories3);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    $tables = [
      'category',
      'room_param',
      'product_property',
      'unit_of_work',
      'unit_of_material',
      'stage',
      'manufacturer',
    ];

    foreach ($tables as $table) {
      Schema::dropIfExists($table);
    }

    DB::statement('DROP EXTENSION IF EXISTS "uuid-ossp"');
  }
}
