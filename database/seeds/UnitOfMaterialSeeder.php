<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */


class UnitOfMaterialSeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = 100;
  public const TEST_TITLE = 'Unit of material title';
  public const TEST_DUPLICATE_TITLE = 'шт';

	public function run() {
		DB::table('unit_of_material')->insert(['id' => self::TEST_ID, 'title' => self::TEST_TITLE]);
	}
}
