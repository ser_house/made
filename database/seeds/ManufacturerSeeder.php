<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */


class ManufacturerSeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = 100;
  public const TEST_TITLE = 'Manufacturer title';
  public const TEST_DUPLICATE_TITLE = 'Knauf';

	public function run() {
    DB::table('manufacturer')->insert(['id' => self::TEST_ID, 'title' => self::TEST_TITLE]);
	}
}
