<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {
    $this->call(UnitOfMaterialSeeder::class);
    $this->call(UnitOfWorkSeeder::class);
    $this->call(StageSeeder::class);
    $this->call(ManufacturerSeeder::class);
    $this->call(RoomParamSeeder::class);
    $this->call(ProductPropertySeeder::class);
    $this->call(CategorySeeder::class);
  }
}
