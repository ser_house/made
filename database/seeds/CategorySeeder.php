<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */


class CategorySeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = '1732aeaa-4972-425e-b112-02a2045f2149';
  public const TEST_TITLE = 'Категория';
  public const TEST_PARENT_ID = '0fea7b1e-da0e-4d9f-a2e0-fcb2555e84ed';

	public function run() {
    DB::table('category')->insert([
      'id' => self::TEST_ID,
      'title' => self::TEST_TITLE,
      'parent_id' => self::TEST_PARENT_ID]);
	}
}
