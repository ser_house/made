<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */


class RoomParamSeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = '1732aeaa-4972-425e-b112-02a2045f2149';
  public const TEST_TITLE = 'Room param title';
  public const TEST_UNIT = 'м2';

	public function run() {
    $unit_id = DB::table('unit_of_work')->where('title', self::TEST_UNIT)->value('id');

    DB::table('room_param')->insert([
      'id' => self::TEST_ID,
      'title' => self::TEST_TITLE,
      'unit_of_work_id' => $unit_id]);
	}
}
