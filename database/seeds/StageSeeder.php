<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */


class StageSeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = 100;
  public const TEST_TITLE = 'Stage title';
  public const TEST_DUPLICATE_TITLE = '0: Демонтаж';

	public function run() {
    DB::table('stage')->insert(['id' => self::TEST_ID, 'title' => self::TEST_TITLE]);
	}
}
