<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 11.02.2020
 * Time: 10:38
 */

class ProductPropertySeeder extends \Illuminate\Database\Seeder {

  public const TEST_ID = '2d98535a-bb14-4471-908f-39b6cb65ab01';
  public const TEST_TITLE = 'Жесткость, H/м';
  public const TEST_TYPE = Core\Attribute\ProductPropertyType::DECIMAL;

  public const TEST_ID_2 = 'ea356684-f86d-4573-87d1-8c0db2a561ed';
  public const TEST_TITLE_2 = 'Мягкость, груша';


  public function run() {
    DB::table('product_property')->insert([
      'id' => self::TEST_ID,
      'title' => self::TEST_TITLE,
      'type' => self::TEST_TYPE
    ]);
    DB::table('product_property')->insert([
      'id' => self::TEST_ID_2,
      'title' => self::TEST_TITLE_2,
      'type' => self::TEST_TYPE
    ]);
  }
}
